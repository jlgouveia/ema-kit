var EMA = EMA || {};
var debug = process.argv.indexOf('--debug') !== -1 ? true : false;

EMA.NODE = (function() {

    var fs = require('fs'),
        fsx = require('fs-extra'),
        mkdir = require('mkp'),
        ev = require('events'),
        call = new ev.EventEmitter();

    var config = require('./ema.config');

    return {
        init: function() {
            console.log(config);
        },
        copy: function() {
            var self = this;

            fs.stat(config.ROOT + '.gitignore', function(err, stats) {
                if (err) {
                    fsx.copy('.gitignore', config.ROOT + '.gitignore', function(error) {
                        if (error) {
                            return console.log('[ERROR 1.1]: ' + error);
                        } else {
                            console.log('Git Ignore Moved!');
                        }
                    });
                    return;
                }

                if (stats.isFile()) {
                    console.log('Config: Git Ignore Found!');
                }
            });

            fs.stat(config.ROOT + '.eslintrc.js', function(err, stats) {
                if (err) {
                    fsx.copy('.eslintrc.js', config.ROOT + '.eslintrc.js', function(error) {
                        if (error) {
                            return console.log('[ERROR 1.2]: ' + error);
                        } else {
                            console.log('JS Lint Moved!');
                        }
                    });
                    return;
                }

                if (stats.isFile()) {
                    console.log('Config: JS Lint Found!');
                }
            });

            fs.stat(config.EMA + 'server/', function(err, stats) {
                if (err) {
                    fsx.copy('server/', config.EMA + 'server/', function(error) {
                        if (error) {
                            return console.log('[ERROR 1.3]: ' + error);
                        } else {
                            console.log('** EMA Server Copiado. **');
                            call.emit('server');
                        }
                    });
                    return;
                }

                if (stats.isDirectory()) {
                    console.log('Server: Folder Found!');
                    call.emit('server');
                }
            });

            call.on('server', function() {
                self.create();
            });
        },
        create: function() {

            var self = this;

            mkdir(config.STRUCTURE, function(error) {
                if (error) {
                    return console.log('[ERROR]: ' + error);
                }

                console.log('Create: Build Folder System with Success!');
            });

            fs.stat(config.VIEWS + 'layouts/default.html', function(err, stats) {
                if (err) {
                    fsx.copy('template/', config.SRC, function(error) {
                        if (error) {
                            return console.log('[ERROR]: ' + error);
                        } else {
                            console.log('** Default template Copy! **');
                        }
                    });
                    return;
                }

                if (stats.isFile()) {
                    console.log('Template: structure Found!');
                }
            });

            fs.stat(config.ROOT + 'package.json', function(err, stats) {
                if (err) {
                    fsx.writeJson(config.ROOT + 'package.json', config.DEPENDENCIES, function(error) {
                        if (error) {
                            return console.log('[ERROR]: ' + error);
                        }

                        console.log('Create file: package.json');
                    });
                    return;
                }

                if (stats.isFile()) {
                    console.log('File: package.json Found!');
                }
            });
        }
    };
});

debug ? EMA.NODE().init() : EMA.NODE().copy();

**INIT**

!HOW TO USE!
- Server: npm run deploy; gulp build --production --php53;
- Local: npm run deploy; and choose one command above!

***USING NODE***

* npm run global * = install gulp
* npm run ema * = copy structure
* npm run deploy * = install dependencies & copy structure

* npm run gulp * = run gulp with watch
* npm run build * = run gulp without server and watch
* npm run build * = run gulp in production without server and watch
* npm run html * = run gulp in production with html file in root without server and watch
* npm run watch * = run watch only without process any function only if change files
* npm run server * = run gulp with server and watch

**GULP**

* gulp = 'build + watch'
* gulp build
* gulp watch
* gulp server

[FLAGS for GULP]
--production = minify and remove sourcemaps
--html = ignore CMS and put .html files in root
--php53 = add code line to .htaccess for support php 5.3

call: $ gulp server


***USING BASH***

call: $ bash ema.sh

ENV mode:

* **-d** = deploy
* **-b** = build

exemple: bash ema.sh -d

* [g] sudo npm i -g npm gulp; *"Instalação Global!"*
* [b] npm cache clean; npm i --production --silent; node structure.js; *"Estrutura e Dependencias Instaladas!"*
* [u] node structure.js; *"Config Actualizado!"*
* [f] sudo chown -R $(whoami) $ROOT; *"Permissões Alteradas!"*
* [p] sudo npm i -g gulp; npm update; *"Packages Actualizados!"*
* [no] *"Cancelado!"*

select: **b**



**ISSUE**

- [x] START!!!
- [x] Write Files PHP
- [x] Get a config file 'awesome'
- [x] 'contents' in 'dist' not remove
- [x] Gulp install bower
- [x] Env inline bash
- [ ] Detect Root (if is ../ ou / )
- [ ] Select if use EMA or not
- [ ] datebase json to pages when is HTML without EMA
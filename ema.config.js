
let ema;
// Get Info from Custom Setting in Packages.json Project
try {
    ema = require('../package.json').ema || {};
} catch (e) {
    ema = { 'info': 'not found', 'error': e };
}

const dir = __dirname.split('/').slice(-2)[0];

let config = {};

config.DIR        = dir;
config.ROOT       = ema.ROOT       || '../';
config.FILES      = ema.FILES      || {};

config.DIST       = ema.DIST       || config.ROOT + 'dist/';
config.SRC        = ema.SRC        || config.ROOT + 'src/';
config.OUTPUT     = ema.OUTPUT     || config.DIST + 'frontend/';
config.TEMPLATES  = ema.TEMPLATES  || config.OUTPUT + 'templates/';
config.VIEWS      = ema.VIEWS      || config.SRC + 'views/';
config.ASSETS     = ema.ASSETS     || config.SRC + 'assets/';
config.EMA        = ema.EMA        || config.SRC + 'ema/';
config.VENDOR     = ema.VENDOR     || config.ROOT + 'node_modules/';

config.MAIN_JS    = ema.MAIN_JS    || config.ASSETS + 'js/main.js';
config.MAIN_SASS  = ema.MAIN_SASS  || config.ASSETS + 'scss/main.scss';

config.STRUCTURE  = config.SRC + '{assets/{img/{images,contents},js/{components,vendor,custom},scss/{components,pages,partials,structure},fonts},views/{pages,partials,layouts,helpers,data},ema/{managers,engine,server}}';

config.DEPENDENCIES = {
    'name': config.DIR,
    'author': '@EMA',
    'dependencies': {
        'blazy': '^1.8.2',
        'bourbon': '^4.3.3',
        'foundation-sites': '^6.3.1',
        'jquery': '^3.2.1',
    }
};

// MAMP VHOST
config.VHOST_DOMAIN = ema.VHOST_DOMAIN || '';
config.SERVER_PORT  = ema.SERVER_PORT || 8000;

config.SERVER_PROXY = {
    host           : config.VHOST_DOMAIN,
    proxy          : config.VHOST_DOMAIN,
    logLevel       : 'debug',
    logConnections : true,
    logFileChanges : true,
    logPrefix      : 'SERVER',
    open           : 'external',
    notify         : true
};

config.SERVER_LOCAL = {
    server         : config.DIST,
    port           : config.SERVER_PORT,
    logLevel       : 'debug',
    logConnections : true,
    logFileChanges : true,
    logPrefix      : 'SERVER',
    open           : true,
    notify         : true
};

config.AUTOPREFIXER = {
    add: true,
    browsers: [
        'ie >= 10',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
    ],
    cascade: false
};

config.COPY = ema.COPY || [
    config.ASSETS + 'fonts/**/*.*'
];

config.DELETE = config.OUTPUT + '{!contents,css,engine,fonts,images,js,managers}/**/*';
config.TEMPLATE_FILES = config.VIEWS + 'pages/**/*.{html,php,hbs,handlebars}';
config.IMAGES = config.ASSETS + 'img/';
config.SASS = ema.SASS || [
    config.VENDOR + 'foundation-sites/scss',
    config.VENDOR + 'bourbon/app/assets/stylesheets'
];

config.JAVASCRIPT = ema.JAVASCRIPT || [
    config.FILES.JS
];

config.BUNDLE = ema.BUNDLE || [
    config.VENDOR + 'jquery/dist/jquery.js',
    config.VENDOR + 'jquery/external/sizzle/dist/sizzle.js',
    config.VENDOR + 'what-input/dist/what-input.js',
    config.VENDOR + 'blazy/blazy.js',
    config.VENDOR + 'foundation-sites/dist/js/foundation.js',
    config.ASSETS + 'js/components/*.js',
    config.ASSETS + 'js/vendor/*.js',
];

config.PLUGINS_HTML = {
    collapseWhitespace            : false,
    removeScriptTypeAttributes    : false,
    removeStyleLinkTypeAttributes : false,
    removeComments                : true,
    processConditionalComments    : false,
    minifyJS                      : true,
    minifyCSS                     : true,
    collapseBooleanAttributes     : false,
    removeAttributeQuotes         : false,
    removeRedundantAttributes     : false,
    removeEmptyAttributes         : false,
    removeOptionalTags            : false
};

config.PLUGINS_PANINI = {
    root     : config.VIEWS + 'pages/',
    layouts  : config.VIEWS + 'layouts/',
    partials : config.VIEWS + 'partials/',
    helpers  : config.VIEWS + 'helpers/',
    data     : config.VIEWS + 'data/'
};

module.exports = config;
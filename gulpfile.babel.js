'use strict';

// Import Services
import gulp from 'gulp';
import plugins from 'gulp-load-plugins';
import del from 'del';
import sequence from 'run-sequence';
import browser from 'browser-sync';
import yargs from 'yargs';
import panini from 'panini';

// Import Config EMA
import config from './ema.config';

// Start Loader Gulp Plugins
const $ = plugins();

// Check for --php53 flag
const isPHP53 = !!(yargs.argv.php53);

// Check for --production flag
const isProduction = !!(yargs.argv.production);

// Check for --html flag
const isHTML = !!(yargs.argv.html);

// Config Server reload for gulp watch
const RELOAD = browser.reload;

// Cache Timestamp
const cached = Date.now();

// Delete the "PATHS.delete" folder
// This happens every time a build starts
gulp.task('clean', (done) => {
    return del([config.DELETE, config.DIST + '*.html'], {
        force: true
    }, done);
    // DEBUG .then((e) => { return $.notify().write('Deleted files and folders: ' + e); })
});

// NPM INSTALL DEPENDENCIES
gulp.task('dependencies', () => {
    return gulp.src([config.ROOT + 'package.json'])
        .pipe($.install({
            args: ['--silent']
        }));
});

// Move PHP files
gulp.task('ema:engine', () => {
    return gulp.src(config.EMA + '{engine,managers}/**', { base: config.EMA })
        .pipe(gulp.dest(config.OUTPUT));
});

// Move Server files
gulp.task('ema:server', () => {
    return gulp.src(config.EMA + 'server/**', { base: config.EMA + 'server/' })
        .pipe(gulp.dest(config.DIST));
});

// Move PHP files from Module
gulp.task('ema:module', () => {
    return gulp.src('./ema/**/*', { base: './ema/' })
        .pipe(gulp.dest(config.DIST + 'ema/'));
});

// .htaccess add php53
gulp.task('ema:php', () => {
    return gulp.src(config.EMA + 'server/**/.htaccess')
        .pipe($.if(isPHP53, $.injectString.append('\nAddHandler application/x-httpd-php53 .php')))
        .pipe(gulp.dest(config.DIST));
});

// Copy files out of the assets folder
gulp.task('copy', () => {
    return gulp.src(config.COPY, { base: config.ASSETS })
        .pipe(gulp.dest(config.OUTPUT));
});

// Copy page templates into finished HTML files
gulp.task('pages', () => {

    panini.refresh();

    return gulp.src(config.TEMPLATE_FILES)
        .pipe(panini(config.PLUGINS_PANINI))
        .pipe($.inject(gulp.src(config.OUTPUT + 'js/*', { read: false }), {
            name: 'main',
            ignorePath: config.ROOT + 'dist',
            removeTags: isProduction,
            // transform: function(filepath) {
            //     return '<script src="' + filepath + '?v=' + Date.now() + '"></script>';
            // }
        }))
        .pipe($.inject(gulp.src(config.OUTPUT + 'css/*', { read: false }), {
            name: 'style',
            ignorePath: config.ROOT + 'dist',
            removeTags: isProduction,
            // transform: function(filepath) {
            //     return '<link rel="stylesheet" href="' + filepath + '?v=' + Date.now() + '">';
            // }
        }))
        // .pipe($.if(isHTML, $.removeCode({
        //     html: true
        // }), $.removeCode({
        //     php: true
        // })))
        .pipe($.if(isProduction, $.htmlmin(config.PLUGINS_HTML)))
        .pipe($.if(isHTML, $.rename({
            extname: '.html'
        }), $.rename({
            extname: '.php'
        })))
        .pipe($.if(isHTML, gulp.dest(config.DIST), gulp.dest(config.TEMPLATES)));
});

// Compile Sass into CSS
// In production, the CSS is compressed
gulp.task('sass', () => {

    let minifycss = $.if(isProduction, $.cleanCss());
    // const cleanCSS = require('gulp-clean-css');
    return gulp.src(config.MAIN_SASS)
        .pipe($.sourcemaps.init())
        .pipe($.plumber())
        .pipe($.sass.sync({
            precision: 10,
            includePaths: config.SASS
        }).on('error', (e) => {
            $.notify().write(e);
        }))
        .pipe($.autoprefixer(config.AUTOPREFIXER))
        .pipe($.base64({
            maxImageSize: 8 * 1024
        }))
        // .pipe($.rev())
        .pipe($.rename({
            suffix: '',
            extname: '.min.css'
        }))
        .pipe(minifycss)
        .pipe($.if(!isProduction, $.sourcemaps.write('./')))
        .pipe(gulp.dest(config.OUTPUT + 'css'))
        .pipe($.size({
            title: 'CSS size:'
        }));
});

// Combine JavaScript into one file
// In production, the file is minified
gulp.task('javascript', () => {

    let uglify = $.if(isProduction, $.uglify({ compress: true })
        .on('error', (e) => {
            $.notify().write(e);
        }));

    return gulp.src(config.MAIN_JS)
        .pipe($.sourcemaps.init())
        .pipe($.plumber())
        .pipe($.babel({ presets: ['babel-preset-es2015'].map(require.resolve) }))
        .pipe($.concat('main.min.js'))
        // .pipe($.rev())
        .pipe($.if(isProduction, $.stripComments()))
        .pipe(uglify)
        .pipe($.if(!isProduction, $.sourcemaps.write('./')))
        .pipe(gulp.dest(config.OUTPUT + 'js'))
        .pipe($.size({
            title: 'JS size:'
        }));
});

// Combine All Plugins JavaScript into one file
// In production, the file is minified
gulp.task('bundle', () => {

    let uglify = $.if(isProduction, $.uglify()
        .on('error', (e) => {
            $.notify().write(e);
        }));

    return gulp.src(config.BUNDLE)
        .pipe($.sourcemaps.init())
        .pipe($.plumber())
        .pipe($.concat('bundle.min.js'))
        // .pipe($.rev())
        .pipe($.stripComments())
        .pipe(uglify)
        .pipe($.if(!isProduction, $.sourcemaps.write('./')))
        .pipe(gulp.dest(config.OUTPUT + 'js'))
        .pipe($.size({
            title: 'Bundle JS size:'
        }));
});

// Copy images to the "dist" folder
// In production, the images are compressed
gulp.task('images', () => {

    let imagemin = $.if(isProduction, $.imagemin({
        progressive: true,
        interlaced: true
    }));

    return gulp.src(config.IMAGES + '**/*.*')
        .pipe(imagemin)
        .pipe(gulp.dest(config.OUTPUT));
});

// Init Structure
gulp.task('default', ['build', 'watch'], (done) => {
    done();
});

// Init EMA
gulp.task('ema', ['ema:module', 'ema:server', 'ema:engine', 'ema:php'], (done) => {
    done();
});

// Build the site
gulp.task('build', (done) => {
    sequence('clean', ['ema', 'copy', 'dependencies', 'images'], ['sass', 'bundle', 'javascript'], 'pages', done);
});

// Watch for file changes
gulp.task('watch', () => {

    gulp.watch(config.COPY, ['copy', RELOAD]);

    gulp.watch([config.VIEWS + 'pages/**/*.{html,php}'], ['pages', RELOAD]);
    gulp.watch([config.VIEWS + '{layouts,partials,helpers,data}/**/*.{html,php,json,yml,js}'], ['pages', RELOAD]);

    gulp.watch([config.EMA + '{engine,managers}/**/*.php'], ['ema:engine', RELOAD]);
    gulp.watch([config.EMA + 'server/**'], ['ema:server', RELOAD]);
    gulp.watch(['./ema/**/*'], ['ema:module', RELOAD]);

    gulp.watch([config.ASSETS + 'scss/**/*.scss'], ['sass', RELOAD]);
    gulp.watch(config.MAIN_JS, ['javascript', RELOAD]);
    gulp.watch(config.BUNDLE, ['bundle', RELOAD]);
    gulp.watch([config.ASSETS + 'img/**/*'], ['images', RELOAD]);

});

// Build the site, run the server with browser-sync for LiveReload, and watch for file changes
gulp.task('server', ['build', 'watch'], () => {

    if (config.VHOST_DOMAIN != '') {
        browser.init(config.SERVER_PROXY);
    } else {
        browser.init(config.SERVER_LOCAL);
    }

});

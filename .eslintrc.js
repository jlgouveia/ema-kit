module.exports = {
    'extends': 'eslint:recommended',
    'ecmaVersion': 6,
    'globals': {
        'document': true,
        'navigator': true,
        'window': true,
        '$': true,
        'TweenLite': true,
        'TweenMax': true,
        'Foundation': true,
        'moment': true,
    },
    'parserOptions': {
        'sourceType': 'module'
    },
    'ecmaFeatures': {
        'modules': true,
        'spread': true,
        'restParams': true
    },
    'env': {
        // I write for browser
        'browser': true,
        // in CommonJS
        'node': true,
        // ES2015
        'es6': true,
        'builtin': true,
        // Jquery
        'jquery': true
    },
    'rules': {
        // No error console
        'no-console': 'off',
        'linebreak-style': [
            'error',
            'unix'
        ],
        'quotes': [2, 'single', {
            'avoidEscape': true,
            'allowTemplateLiterals': true
        }],
        'semi': [
            'error',
            'always'
        ]
    }
};

<?php
    /* Database config */
    define('CONFIGHOST', 'localhost');
    define('CONFIGDATABASE', '');
    define('CONFIGUSERNAME', 'joseluis');
    define('CONFIGPASSWORD', '08wiBY');

    define('DEVHOST', 'localhost');
    define('DEVDATABASE', '');
    define('DEVUSERNAME', 'root');
    define('DEVPASSWORD', 'root');
    define('ROOTPATH', dirname(__FILE__));

    // MAILCHIMP API KEY IF EXISTS
    define('MAILCHIMP_API_KEY', '');
    // ALLOW USER ACCESS
    define('ALLOW_GET_USERS', true);
    define('ALLOW_ADD_CONTENT_NO_SESSION', true);
    define('ALLOW_ADD_USERS_RELATIONS', true);

    define('TEMPLATE_NOLOGIN_FALLBACK', 'default.php');
    define('TEMPLATE_DEFAULT_FILE', 'file.php');
